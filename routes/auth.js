var express = require("express");
const { auth } = require("../lib/firebase-service");
var admin = require("../lib/firebase-service");
var { PetOwner, PetWalker } = require("../models/models");
var router = express.Router();

router.post("/signup/petowner", function (req, res, next) {
  const idToken = req.headers.authorization.split(" ")[1]
  if (idToken === undefined) return res.status(401).send("idToken not provided");
  const latitude = req.body.latitude===undefined?4.60:req.body.latitude;
  const longitude = req.body.longitude===undefined?-74.60:req.body.latitudee;
  const location = { type: 'Point', coordinates: [parseFloat(longitude), parseFloat(latitude)]};
  admin
    .auth()
    .verifyIdToken(idToken)
    .then((decodedToken) => {
      const uid = decodedToken.uid;
      admin
        .auth()
        .setCustomUserClaims(uid, { petOwner: true })
        .then(() => {
          admin
            .auth()
            .getUser(uid)
            .then((user) => {
              PetOwner.upsert({
                uid: uid,
                displayName: user.displayName,
                photoURL: user.photoURL,
                currentLocation: location
              }).then((result) => {
                res.send(result[0]);
              }).catch((error) =>{
                res.status(500).send(error);
              });
            });
        });
    })
    .catch((error) => {
      res.status(401).send();
    });
});

router.post("/signup/petwalker", function (req, res, next) {
    const idToken = req.headers.authorization.split(" ")[1];
    const description = req.body.description;
    const latitude = req.body.latitude===undefined?4.60:req.body.latitude;
    const longitude = req.body.longitude===undefined?-74.60:req.body.longitude;
    const location = { type: 'Point', coordinates: [parseFloat(longitude), parseFloat(latitude)]};
    if (description === undefined) return res.status(412).send("You need to give a description");
    if (idToken === undefined) return res.status(401).send();
    admin
      .auth()
      .verifyIdToken(idToken)
      .then((decodedToken) => {
        const uid = decodedToken.uid;
        admin
          .auth()
          .setCustomUserClaims(uid, { petWalker: true })
          .then(() => {
            admin
              .auth()
              .getUser(uid)
              .then((user) => {
                PetWalker.upsert({
                  uid: uid,
                  displayName: user.displayName,
                  photoURL: user.photoURL,
                  description: description,
                  available: false,
                  currentLocation: location
                }
                ).then((result) => {
                  res.send(result[0]);
                }).catch((error) =>{
                  console.log(error)
                  res.status(500).send(error);
                });
              });
          });
      })
      .catch((error) => {
        res.status(401).send(error);
      });
  });

module.exports = router;
