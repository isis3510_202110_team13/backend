var express = require("express");
var admin = require("../lib/firebase-service");
const { PetOwner, PetWalker, Tag, Pet } = require("../models/models");
const sequelize = require("../lib/sequelize");
const { Sequelize} = require("sequelize");
var router = express.Router();

router.post("/", function (req, res, next) {
  PetWalker.upsert(req.body).then((result) => res.send(result[0]));
});

/* GET petWalkers listing. */
router.get("/", function (req, res, next) {
  let petOwnerUid = req.query.petOwnerUid;
  let long = req.query.long;
  let lat = req.query.lat;
  let distance = req.query.distance;
  let matched = req.query.matched === 'true';
  let orderBy = req.query.orderBy;
  let limit = req.query.limit;

  let order;
  if (orderBy == "distance") {
    order = [[Sequelize.col("distance"), "ASC"]];
  } else if (orderBy == "avgRating") {
    order = [["avgRating", "DESC"]];
  }

  let matchedInclude;
    if(petOwnerUid != undefined && matched){
      matchedInclude = [{
        model: Pet,
        attributes: [],
        where: {
          PetOwnerUid: petOwnerUid
        },
        required: true
      }];
  }

  if (lat != undefined && long != undefined) {
    let where;
    if (distance != undefined) {
      where = [
        Sequelize.where(
          Sequelize.fn(
            "ST_DWithin",
            Sequelize.col("currentLocation"),
            Sequelize.fn( "ST_MakePoint", long, lat),
            distance
          ),
          true
        ),
      ];
    }

    PetWalker.findAll({
      attributes: {
        include: [
          [
            Sequelize.fn(
              "ST_Distance",
              Sequelize.col("currentLocation"),
              Sequelize.fn("ST_MakePoint", long, lat)
            ),
            "distance",
          ],
        ],
      },
      include: [
        {
          model: Tag,
          through: {
            attributes: []
          },
          include: matchedInclude,
          required: matched
        },
      ],
      where: where,
      limit: limit,
      order: order,
    })
      .then((result) => {
        res.send(result);
      })
      .catch((error) => {
        res.status(500).send(error);
      });
  } else {
    PetWalker.findAll({
      limit: limit,
      order: order,
      include: [
        {
          model: Tag,
          through: {
            attributes: []
          },
          include: matchedInclude,
          required: matched
        },
      ]
    })
      .then((result) => {
        res.send(result);
      })
      .catch((result) => {
        console.log(result);
      });
  }
});

router.get("/:id", function (req, res, next) {
  const uid = req.params.id;
  PetWalker.findByPk(uid, {
    include: [
      {
        model: Tag,
      },
    ],
  }).then((result) => res.send(result));
});

module.exports = router;
