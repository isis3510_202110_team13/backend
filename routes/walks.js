var express = require("express");
var admin = require("../lib/firebase-service");
const { Sequelize} = require("sequelize");
const sequelize = require("../lib/sequelize");
const { Walk, Pet, PetWalker } = require("../models/models");
var router = express.Router();

router.get("/:id", function (req, res, next) {
  Walk.findByPk(req.params.id, {
    include: [
      {
        model: PetWalker,
      },
      {
        model: Pet,
      },
    ],
  }).then((result) => res.send(result))
  .catch((error) => {
    console.log(error);
    res.status(500).send(error);
  });
});

router.post("/", function (req, res, next) {
  Walk.create(req.body)
  .then((result) => res.send(result))
  .catch((error) => {
    console.log(error);
    res.status(500).send(error);
  });
});

router.put("/:id", function (req, res, next) {

  let walk;
  if( req.body.status == 'started'){
    walk = {...req.body, startTime: Sequelize.fn('NOW')};
  }else if(req.body.status == 'finished'){
    walk = {...req.body, endTime: Sequelize.fn('NOW')};
  }else{
    walk = req.body;
  }
  Walk.update(walk, {
    where: {
      id: req.params.id,
    },
    returning: true
  }).then((result) => {
    if (result[0] === 0)
      return res.status(404).send("The walk with the given id was not found.");

    if (result[1][0].status == "finished") {
      PetWalker.findOne({
        where:{
          uid: result[1][0].PetWalkerUid
        },
        attributes: [
          [ Sequelize.fn("AVG", Sequelize.col("Walks.rating")), "rating"],
          [ Sequelize.col("PetWalker.uid"), "uid"]
        ],
        include: [
          {
            model: Walk,
            attributes: []
          },
        ],
        group: ["PetWalker.uid"],
      }).then((result) => {
        console.log(result);
        PetWalker.update(
          { avgRating: result.dataValues.rating },
          {
            where: {
              uid: result.dataValues.uid,
            },
          }
        ).then((result) => {
          return res.send("The Walk has been updated");
        }
        ).catch((error) => {
          console.log(error);
          return res.status(500).send(error);
        });
      }).catch((error) => {
        console.log(error);
        return res.status(500).send(error);
      });
    }else{
      return res.send("The Walk has been updated");
    }
  
  });
});
module.exports = router;
