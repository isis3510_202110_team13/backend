var express = require("express");
const sequelize = require("../lib/sequelize");
const {Sequelize} = require("sequelize");
const { Pet, PetOwner, Tag, Walk, Panic, PetWalker } = require("../models/models");
const { transaction } = require("../lib/sequelize");
var router = express.Router();

router.post("/", function (req, res, next) {
  PetOwner.upsert(req.body)
  .then((result) => res.send(result[0]))
  .catch((error)=>{
    console.log(error);
    res.status(500).send(error);
  });
});

router.get("/:uid", function (req, res, next) {
  PetOwner.findByPk(req.params.uid, {
    include: [
      {
        model: Pet
      },
    ],
  }).then((result) => {
    if (result === null)
      res.status(404).send("The Pet Owner with the given uid was not found.");
    res.send(result);
  }).catch((error) => {
    console.log(error);
    res.status(500).send(error);
  })
});

router.put("/:uid", function (req, res, next) {
  PetOwner.update(req.body, {
    where: {
      uid: req.params.uid,
    },
  })
    .then((result) => {
      if (result[0] !== 0) res.send("The Pet Owner was updated");
      else
        res.status(404).send("The Pet Owner with the given uid was not found.");
    })
    .catch((error) => {
      console.log(error);
      res.status(500).send(error);
    });
});

router.get("/:uid/pets", function (req, res, next) {
  Pet.findAll({
    where: {
      PetOwnerUid: req.params.uid,
    },
    include: [
      {
        model: Tag,
        through: {
          attributes: []
        }
      },
    ]
  }).then((result) => {
    res.send(result);
  }).catch((error) =>{
    console.log(error);
    res.status(500).send(error);
  });
});

router.post("/:uid/pets", function (req, res, next) {
  console.log(req.body);
  Tag.bulkCreate(req.body.Tags,{ignoreDuplicates: true});

  Pet.create(
      { ...req.body, PetOwnerUid: req.params.uid }
    ).then((result) => {
      result.addTags(req.body.Tags.map(e => e.name));
      res.send(result);
    }).then(result => {
      res.send(result);
    }).catch((error) => {
      console.log(error);
      res.status(500).send(error);
    });
});

router.put("/:PetOwnerUid/pets/:petId", function (req, res, next) {
  Pet.update(
    req.body,
    {
      where:{
        id: req.params.petId
      }
    },
    {
      include: [{model: Tag}]
    }
  ).then((result) => {
    if (result[0] === undefined)
      res.status(404).send("The Pet Owner with the given uid was not found.");
    res.send("The Pet has been updated");
  }).catch((error) => {
    console.log(error);
    res.status(500).send(error);
  })
});

router.get("/:uid/walks", function (req, res, next) {
  Walk.findAll({
    include: [
      {
        model: Pet,
        where:{
          PetOwnerUid: req.params.uid
        }
      },
      {
        model: PetWalker
      }
    ]
  }).then((result) => {
    res.send(result);
  }).catch((error)=>{
    console.log(error);
    res.status(500).send(error);
  })
});

router.post("/:uid/panics", function (req, res, next){
  Panic.create( {PetOwnerUid: req.params.uid })
  .then((result) => {
    res.send(result);
  })
  .catch((error) => {
    console.log(error);
    res.status(500).send(error);
  })
});

router.get("/:uid/panics", function (req, res, next){
  Panic.findAll( {
    where:{
      PetOwnerUid: req.params.uid
    }
  })
  .then((result) => {
    res.send(result);
  })
  .catch((error) => {
    console.log(error);
    res.status(500).send(error);
  })
});

module.exports = router;
