const { DataTypes, Model } = require("sequelize");
const sequelize = require("../lib/sequelize");


class PetWalker extends Model {}

PetWalker.init(
  {
    uid: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    displayName: {
      type: DataTypes.STRING
    },
    photoURL: {
      type: DataTypes.STRING
    },
    description: {
        type: DataTypes.TEXT,
    },
    avgRating: {
      type: DataTypes.DOUBLE,
      defaultValue: 0
    },
    currentLocation: DataTypes.GEOGRAPHY,
    city: {
      type: DataTypes.STRING
    },
    available: DataTypes.BOOLEAN,
  },
  {
    sequelize,
    timestamps: false
  }
);

//PetWalker.hasMany(Walk);
//PetWalker.belongsToMany(Tag, { through: 'PetWalkerTags' });


//PetWalker.sync();

module.exports = PetWalker;