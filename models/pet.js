const { DataTypes, Model } = require("sequelize");
const sequelize = require("../lib/sequelize");
const PetOwner = require("./petOwner");
const Walk = require("./walk");
const Tag = require("./tag");

class Pet extends Model {}

Pet.init(
  {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    breed: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    age: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    size: {
      type: DataTypes.ENUM,
      values: ['small', 'medium', 'large'],
  },
    photoURL: {
      type: DataTypes.STRING,
    },
  },
  {
    sequelize,
    timestamps: false
  }
);

//Pet.belongsTo(PetOwner);
//Pet.hasMany(Walk);
//Pet.belongsToMany(Tag, { through: 'PetTags' });

//Pet.sync();

module.exports = Pet;
