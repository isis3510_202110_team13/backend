const { Sequelize, DataTypes, Model } = require("sequelize");
const sequelize = require("../lib/sequelize");
const PetWalker = require("./petWalker");
const PetOwner = require("./petOwner");
const Pet = require("./pet");
const Walk = require("./walk");
const Tag = require("./tag");
const Panic = require("./panic")
const { findAll } = require("./petWalker");

PetOwner.hasMany(Pet);
Pet.belongsTo(PetOwner,{
    constraints: false,
});

PetOwner.hasMany(Panic);
Panic.belongsTo(PetOwner,{
    constraints: false,
});

Pet.hasMany(Walk);
Walk.belongsTo(Pet, {
    constraints: false,
});

Pet.belongsToMany(Tag, { through: 'PetTags'});
Tag.belongsToMany(Pet, { through: 'PetTags'});



PetWalker.hasMany(Walk);
Walk.belongsTo(PetWalker, {
    constraints: false,
});

PetWalker.belongsToMany(Tag, { through: 'PetWalkerTags'});
Tag.belongsToMany(PetWalker, { through: 'PetWalkerTags'});




//Creates all the tables with the given constraints 
sequelize.sync();

module.exports = {
    PetOwner,
    PetWalker,
    Pet,
    Walk,
    Tag,
    Panic
};


