const {Model} = require("sequelize");
const sequelize = require("../lib/sequelize");

class Panic extends Model {}

Panic.init({},
{
    sequelize
});

module.exports = Panic;