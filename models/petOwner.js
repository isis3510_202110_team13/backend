const { DataTypes, Model } = require("sequelize");
const sequelize = require("../lib/sequelize");
const Pet = require("./pet");

class PetOwner extends Model {}

PetOwner.init(
  {
    uid: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    displayName: {
      type: DataTypes.STRING
    },
    city: {
      type: DataTypes.STRING
    },
    photoURL: {
      type: DataTypes.STRING
    },
    currentLocation: DataTypes.GEOGRAPHY,
  },
  {
    sequelize,
    timestamps: false
  }
);

//PetOwner.hasMany(Pet);

//PetOwner.sync();

module.exports = PetOwner;
