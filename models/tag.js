const { DataTypes, Model } = require("sequelize");
const sequelize = require("../lib/sequelize");
const Pet = require("./pet");
const PetWalker = require("./petWalker") 

class Tag extends Model {}

Tag.init(
  {
    name: {
        type: DataTypes.STRING,
        primaryKey: true,
    }
  },
  {
    sequelize,
    timestamps: false
  }
);

//Tag.belongsToMany(PetWalker, { through: 'PetWalkerTags' });
//Tag.belongsToMany(Pet, { through: 'PetTags' });
//Tag.sync();

module.exports = Tag;