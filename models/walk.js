const { DataTypes, Model } = require("sequelize");
const { validate } = require("../lib/sequelize");
const sequelize = require("../lib/sequelize");

class Walk extends Model {}

Walk.init(
  {
    startTime: {
        type: DataTypes.DATE,
    },
    endTime: {
        type: DataTypes.DATE,
    },
    status: {
        type: DataTypes.ENUM,
        values: ['hired', 'started', 'finished', 'canceled'],
        defaultValue: 'hired'
    },
    rating:{
        type: DataTypes.DOUBLE,
        validate:{
            min: 1,
            max: 5
        }
    }
  },
  {
    sequelize
  }
);

//Walk.belongsTo(PetWalker);
//Walk.belongsTo(Pet);
//Walk.sync();

module.exports = Walk;