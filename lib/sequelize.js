const { Sequelize } = require("sequelize");
const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, {
    host: process.env.DB_HOST,
    port: 5432,
    logging: console.log,
    dialect: 'postgres'
});

sequelize.authenticate().then(() => {
  console.log("Authenticated!");
});


module.exports = sequelize;