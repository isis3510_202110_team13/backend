INSERT INTO dog_walker(username,avg_rating,tags)
VALUES('ca.tirado',4.25,'{medium, special_care}');

INSERT INTO dog_walker(username,avg_rating,tags)
VALUES('m.escobar',0,'{small}');

INSERT INTO dog_walker(username,avg_rating,tags)
VALUES('d.perilla',2,'{big, bitter}');

INSERT INTO dog_walker(username,avg_rating,tags)
VALUES('am.cano',4.5,'{special_care}');


INSERT INTO dog_owner(username,owner_address)
VALUES('j.tambo','calle 134#11-45')

INSERT INTO dog_owner(username,owner_address)
VALUES('vm.tirado','cra 10a#134b-23')


INSERT INTO pet(dog_owner,breed,tags,pet_name)
VALUES ('vm.tirado','dalmatian','{medium, dalmatian}','baloo');

INSERT INTO pet(dog_owner,breed,tags,pet_name)
VALUES ('j.tambo','pomeranian','{small}','piglet')


INSERT INTO walk(current_position,rating,dog_owner,dog_walker,pet_name)
VALUES('34',5,'vm.tirado','am.cano','baloo');

INSERT INTO walk(current_position,rating,dog_owner,dog_walker,pet_name)
VALUES('53',2,'vm.tirado','d.perilla','baloo');

INSERT INTO walk(current_position,rating,dog_owner,dog_walker,pet_name)
VALUES('23',4,'j.tambo','am.cano','piglet');

INSERT INTO walk(current_position,rating,dog_owner,dog_walker,pet_name)
VALUES('34',4,'j.tambo','ca.tirado','piglet');

INSERT INTO walk(current_position,rating,dog_owner,dog_walker,pet_name)
VALUES('78',4.5,'vm.tirado','ca.tirado','baloo');
