CREATE TABLE dog_walker(
	username VARCHAR(255) NOT NULL,
	avg_rating FLOAT NOT NULL ,
	tags VARCHAR(120)[],
   PRIMARY KEY(username)
);

CREATE TABLE dog_owner(
	username VARCHAR(255) NOT NULL,
	owner_address VARCHAR(255) NOT NULL,
	PRIMARY KEY (username)
);

CREATE TABLE pet(
	pet_name VARCHAR(120) NOT NULL,
	breed VARCHAR(120) NOT NULL,
	tags VARCHAR(120)[],
	dog_owner VARCHAR(255) NOT NULL,
	CONSTRAINT fk_dog_owner
		FOREIGN KEY (dog_owner)
		REFERENCES dog_owner(username),
	PRIMARY KEY (pet_name)
	
);

CREATE TABLE walk(
	id SERIAL NOT NULL,
	current_position FLOAT,
	rating FLOAT,
	dog_owner VARCHAR(255) NOT NULL,
	pet_name VARCHAR(120) NOT NULL,
	dog_walker VARCHAR(255) NOT NULL,
	CONSTRAINT fk_dog_owner
		FOREIGN KEY (dog_owner)
		REFERENCES dog_owner(username),
	CONSTRAINT fk_dog_walker
		FOREIGN KEY (dog_walker)
		REFERENCES dog_walker(username),
	CONSTRAINT fk_petname
		FOREIGN KEY (pet_name)
		REFERENCES pet(pet_name),
	PRIMARY KEY (id)
);
