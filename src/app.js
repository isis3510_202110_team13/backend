const express= require('express');
const app= express();
const morgan=require('morgan');

app.use(morgan('dev'))
//app.use(cors())

//middleware 
app.use(express.json());
app.use(express.urlencoded({extended:false}));

//routes 
app.use(require('./routes/dogWalkers'));
app.use(require('./routes/walks'))

module.exports= app;