const {Router}= require('express');
const router= Router();


const {getWalks,getWalksByDogWalker}=require('../controllers/walk')

router.get('/walks',getWalks)
router.get('/walks/:dog_walker',getWalksByDogWalker)

module.exports=router
