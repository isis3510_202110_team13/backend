const {Router}=require('express');
const router= Router();

const{getDogWalker,getDogWalkerById,createDogWalker,getTopRatedDogWalkers, getMatchedDogWalkers}=require('../controllers/dogWalker')

router.get('/dogwalkers',getDogWalker)
router.post('/dogwalkers',createDogWalker) 
router.get('/dogwalkers/topRated',getTopRatedDogWalkers)
router.get('/dogwalkers/:username',getDogWalkerById)
router.get('/dogwalkers/matched/user?',getMatchedDogWalkers)
module.exports=router