const {Pool}=require('pg');
const { response } = require('../app');

const pool=new Pool({
    host:'localhost',
    user: 'postgres',
    password:'201632317',
    database:'moviles',
    port:'5432'
})

const getDogWalker= async(req,res)=>{
    const response=await pool.query('SELECT * FROM dog_walker')
    console.log(response.rows);
    res.status(200).json(response.rows);
}

const getDogWalkerById= async(req,res)=>{
    const username=req.params.username;
    console.log(username)
    const response=await pool.query('SELECT * FROM dog_walker WHERE username=$1',[username])
    console.log(response.rows);
    res.status(200).json(response.rows);
}

const getTopRatedDogWalkers= async(req,res)=>{
    const response=await pool.query('SELECT * FROM dog_walker ORDER BY avg_rating DESC limit 3',)
    console.log(response.rows);
    res.status(200).json(response.rows);
}

const getMatchedDogWalkers = async(req, res) =>{
    const username=req.query.username;
    const response = await pool.query('SELECT dw.* FROM pet p, dog_owner dow, dog_walker dw WHERE dow.username = $1 AND p.tags && dw.tags', [username]);
    console.log(response.rows);
    res.status(200).json(response.rows);
}
// const resetAvg_rateDogWalker= (req,res)=>{
//     console.log('Hola reset')
//     pool.query('SELECT * FROM dog_walker').then((response)=>{
//         console.log(response.rows);
//         response.rows.forEach( element => {
//         username = element;
//         console.log(username)
//          pool.query('UPDATE dog_walker SET avg_rating = $1 WHERE username=$2',[2,username])
//          .then((response2)=>{
//              console.log(response2.rows)
//             res.status(200).json(response2.rows);
//          });
//         }); 
//     } 

//     );
// }

const createDogWalker= async(req,res)=>{
    const {username,tags}=req.body;
    console.log(username)
    const response=await pool.query('INSERT INTO dog_walker(username,tags) VALUES($1,$2)',[username,tags])
    console.log(response.rows);
    res.send('user created');
}

module.exports={
    getDogWalker,
    createDogWalker,
    getDogWalkerById,
    getTopRatedDogWalkers,
    getMatchedDogWalkers,
}