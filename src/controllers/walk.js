const {Pool}=require('pg');

const pool=new Pool({
    host:'localhost',
    user: 'postgres',
    password:'201632317',
    database:'moviles',
    port:'5432'
})

const getWalks= async(req,res)=>{
    response=await pool.query('SELECT * FROM walk')
    console.log(response.rows);
    res.status(200).json(response.rows);
}

const getWalksByDogWalker= async(req,res)=>{
    const walker=req.params.dog_walker
    response=await pool.query('SELECT * FROM walk WHERE dog_walker=$1',[walker])
    console.log(response.rows);
    res.status(200).json(response.rows);
}


module.exports={
    getWalks,
    getWalksByDogWalker
}

