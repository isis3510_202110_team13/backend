var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
require("dotenv").config();

var authRouter = require('./routes/auth');
var petOwnersRouter = require('./routes/petOwners');
var petWalkersRouter = require('./routes/petWalkers');
var walksRouter = require('./routes/walks');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/auth', authRouter);
app.use('/petowners', petOwnersRouter);
app.use('/petwalkers', petWalkersRouter);
app.use('/walks', walksRouter);



module.exports = app;
